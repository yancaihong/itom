import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    baseColor: window.localStorage.getItem('baseColor')||'#457fca',
    baseColorTheme: window.localStorage.getItem('baseColorTheme')||'#337ab7'
  },
  mutations: {

  },
  actions: {

  }
})
