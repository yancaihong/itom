import zhLocale from 'element-ui/lib/locale/lang/zh-CN';
import homeNav from '../homeNav/zh.js';

const zh =  {
  message: {
      add: '添加',
      search: '搜索'
  },
  ...zhLocale,
  homeNav,
  userInfo: {
    userProfile: '用户个人中心',
    settings: '设置',
    logout: '退出',
    chinese: '中文',
    english: 'English'
  },
  landing:{
    home:'首页',
    features:'特色'
  }
}
export default zh;