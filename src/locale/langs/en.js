import enLocale from 'element-ui/lib/locale/lang/en';
import homeNav from '../homeNav/en.js';

const en = {
  message: {
    add: 'Add',
    search: 'Search'
  },
  ...enLocale,
  homeNav,
  userInfo: {
    userProfile: 'User Profile',
    settings: 'Settings',
    logout: 'Logout',
    chinese: '中文',
    english: 'English'
  },
  landing:{
    home:'home',
    features:'features'
  }
}

export default en;