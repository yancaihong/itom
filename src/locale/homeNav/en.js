const homeNav = {
  systemManagement: {
    name: 'System Management',
    dashboard: 'Dashboard',
    roles: 'Roles',
    users: 'Users',
    department: 'department',
    teams: 'Teams',
    configurations: 'Configurations'
  },
  business:{
    name: 'Business',
    customers: 'Customers',
    providers: 'Providers'
  },
  projectManagement: {
    name: 'Project Management',
    projects: 'Projects',
    purchaseOrders: 'Purchase Orders',
    contracts: 'Contracts',
    projectGraph: 'Project Graph',
  },
  assetsManagement: {
    name: 'Assets Management',
    hardWare: 'HardWare',
    softWares: 'SoftWares',
    licenses: 'Licenses',
    assetInspection: 'Asset Inspection',
    assetTree: 'Asset Tree',
    zone: 'Zone',
  },
  visitorsManage: {
    name: 'Visitors Manage',
    visitorsRequest: 'Visitors Request',
    verifyVisitors: 'Verify Visitors'
  },
  ticketSystem: {
    name: 'Ticket System',
    applyTicket: 'Apply Ticket',
    ticketSupport: 'Ticket Support',
  }
}

export default homeNav;