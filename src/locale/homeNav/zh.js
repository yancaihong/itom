const homeNav = {
  systemManagement: {
    name: '系统管理',
    dashboard: '面板',
    roles: '角色',
    users: '用户',
    department: '部门',
    teams: '组',
    configurations: '配置'
  },
  business:{
    name: '商务',
    customers: '客户',
    providers: '供应商'
  },
  projectManagement: {
    name: '项目管理',
    projects: '项目',
    purchaseOrders: '采购清单',
    contracts: '合同',
    projectGraph: '工程图表',
  },
  assetsManagement: {
    name: '资产管理',
    hardWare: '硬件',
    softWares: '软件',
    licenses: '证书',
    assetInspection: '资产巡检',
    assetTree: '资产树',
    zone: '区域',
  },
  visitorsManage: {
    name: '访客管理',
    visitorsRequest: '访问请求',
    verifyVisitors: '访客审核'
  },
  ticketSystem: {
    name: '工单系统',
    applyTicket: '新建工单',
    ticketSupport: '服务工单',
  }
}

export default homeNav;