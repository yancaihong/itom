import Vue from 'vue';
import App from './App.vue';
import router from './router/index.js';
import store from './store';

// 引入element ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 引入 font-awesome 图标 css
import 'font-awesome/css/font-awesome.css';

// 路由守卫
import '@/permission.js'; // permission control

// storage
import './tools/storage'

// 封装api访问等功能
import tools from './tools/index';
Vue.prototype.$tools = tools;

// 引入提示框
import message from './tools/message'
Vue.prototype.message = message

// 引入多语言切换

import i18n from './locale/i18n.js';

// 导入
import ExportExcel from './demo/table2Excel/excel.js';
Vue.use(ExportExcel)


//定义全局过滤器
import * as filters from './filters'; // global filters
// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key]);
});

// 引入css文件
import './assets/css/common.less';
import './assets/css/ui-common.less';

import './registerServiceWorker'

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')