const image = [{
        key: 'bgimg',
        img: require('@/assets/img/hardwares/landing-main.jpg')
    },
    {
        key: 'loginbgimg',
        img: require('@/assets/img/hardwares/bg-login.jpg')
    },
    {
        key: 'logohopesen',
        img: require('@/assets/img/hardwares/logo-hopesen.png')
    },
    {
        key: 'iconlogin',
        img: require('@/assets/img/hardwares/icon-login.png')
    },

    //-----------------------系统管理图片配置开始----------------------------
    {
        key: 'logo',
        img: require('@/assets/img/sysMan/logo.png')
    }
]
// 图片配置map
export const imageMap = new Map();
for (let item of image) {
    imageMap.set(item.key, item.img);
}