/**
 * 方法名： 判断是否为空
 * @param value
 */
function isNull(value) {
    let flag = 0;
    let gettype = Object.prototype.toString;
    let vtype = gettype.call(value);
    if (vtype === '[object String]') {
      flag = value.trim(value.toString()) === '' || value.trim(value.toString()) === 'undefined' || value.trim(value.toString()) === 'null' ? 1 : 0;
    } else if (vtype === '[object Undefined]') {
      flag = value === undefined ? 1 : 0;
    } else if (vtype === '[object Null]') {
      flag = value === null ? 1 : 0;
    } else if (vtype === '[object Object]') {
      flag = JSON.stringify(value) === '{}' ? 1 : 0;
    } else if (vtype === '[object Array]') {
      flag = value.length === 0 ? 1 : 0;
    }
    return flag;
  }

  export default {
    isNull
  }