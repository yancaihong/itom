import axios from 'axios';
export default {
  //api访问封装
  ajax(uri, type, params) {
      if(type === 'get' && params) {
          if(uri.indexOf('?') === -1) {
              uri = uri + '?'
          } else if(uri.lastIndexOf('&') !== uri.length -1) {
              uri = uri + '&'
          }
          for(let k in params) {
              uri = uri + k + '=' + window.encodeURIComponent(params[k]) + '&';
          }
      }
      return axios[type](uri, params)
  },
  //数据深度复制
  deepCopy(obj) {
    let result = Array.isArray(obj) ? [] : {};
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (typeof obj[key] === 'object' && obj[key]!==null) {
                result[key] = this.deepCopy(obj[key]);   //递归复制
            } else {
                result[key] = obj[key];
            }
        }
    }
    return result;
  },
}