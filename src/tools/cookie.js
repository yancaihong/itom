// 设置cookie
function setCookie(key, value, exdays) {
  var exdate = new Date();
  exdate.setTime(exdate.getTime() + 24 * 60 * 60 * 1000 * exdays); //保存的天数
  document.cookie =
    key + '=' + value + ';path=/;expires=' + exdate.toLocaleString();
}

// 获取cookie
function getCookie(key) {
  if (document.cookie.length > 0) {
    var arr = document.cookie.split('; '); //这里显示的格式需要切割一下自己可输出看下
    // console.log(arr)
    var data = [];
    for (let i = 0; i < arr.length; i++) {
      let arr2 = arr[i].split('='); //再次切割
      if(key===arr2[0]){
        return arr2[1]
      }
    }
    return data;
  }
}
//删除cookie
function clearCookie(key, value) {
  this.setCookie(key, value, -1); //修改2值都为空，天数为负1天就好了
}
export default {
  setCookie,
  getCookie,
  clearCookie
};
