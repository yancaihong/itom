import Vue from 'vue';
import Router from 'vue-router';

import route from './config/index';

Vue.use(Router);

// 捕获同一个路由地址的跳转的报错情况，不然在控制台会报错，但不影响使用
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}


const router = new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    ...route
  ]
});

router.beforeEach((to, from, next) => {
  if(to.meta.title) {
    document.title = to.meta.title
  }
  next();
})

export default router;
