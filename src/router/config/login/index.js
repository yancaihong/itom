const login = () => import( /* webpackChunkName: "Login" */ '@/views/login/login.vue');
const forget = () => import( /* webpackChunkName: "Login" */ '@/views/login/forget.vue');
const signUp = () => import( /* webpackChunkName: "Login" */ '@/views/login/signUp.vue');
const landing = () => import( /* webpackChunkName: "Home" */ '@/views/login/landing.vue');

let logins = [{
    path: '/login',
    name: 'login',
    component: login,
    meta: {
      title: '登录页面'
    }
  },
  {
    path: '/signUp',
    name: 'signUp',
    component: signUp,
    meta: {
      title: '注册页面'
    }
  },
  {
    path: '/forget',
    name: 'forget',
    component: forget,
    meta: {
      title: '找回密码'
    }
  },
  {
    path: '/landing',
    name: 'landing',
    component: landing,
    meta: {
      title: '展示页'
    }
  }
]
export default logins