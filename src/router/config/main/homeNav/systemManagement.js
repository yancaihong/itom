const systemManagement = () => import(/* webpackChunkName: "systemManagement" */ '@/views/main/systemManagement/index.vue');
const configurations = () => import(/* webpackChunkName: "systemManagement" */ '@/views/main/systemManagement/configurations.vue');
const dashboard = () => import(/* webpackChunkName: "systemManagement" */ '@/views/main/systemManagement/dashboard.vue');
const department = () => import(/* webpackChunkName: "systemManagement" */ '@/views/main/systemManagement/department.vue');
const roles = () => import(/* webpackChunkName: "systemManagement" */ '@/views/main/systemManagement/roles.vue');
const teams = () => import(/* webpackChunkName: "systemManagement" */ '@/views/main/systemManagement/teams.vue');
const users = () => import(/* webpackChunkName: "systemManagement" */ '@/views/main/systemManagement/users.vue');
import homeNav from '@/locale/homeNav/en.js';
import homeNav1 from '@/locale/homeNav/zh.js';

let systemManagements = [
  {
    path: '/systemManagement',
    name: 'systemManagement',
    redirect: '/systemManagement/dashboard',
    component: systemManagement,
    meta: {
      title: '首页',
      icon: 'fa-laptop'
    },
    children: [
      {
        path: '/systemManagement/dashboard',
        name: 'dashboard',
        component: dashboard,
        meta: {
          title: '首页',
          icon: 'fa-dashboard'
        }
      },
      {
        path: '/systemManagement/roles',
        name: 'roles',
        component: roles,
        meta: {
          titleEN: homeNav.systemManagement.dashboard,
          titleZH: homeNav1.systemManagement.dashboard,
          icon: 'fa-users'
        },
      },
      {
        path: '/systemManagement/users',
        name: 'users',
        component: users,
        meta: {
          title: '部门',
          icon: 'fa-user'
        },
      },
      {
        path: '/systemManagement/department',
        name: 'department',
        component: department,
        meta: {
          title: '角色',
          icon: 'fa-sitemap'
        },
      },
      {
        path: '/systemManagement/teams',
        name: 'teams',
        component: teams,
        meta: {
          title: '首页',
          icon: 'fa-slideshare'
        },
      },
      {
        path: '/systemManagement/configurations',
        name: 'configurations',
        component: configurations,
        meta: {
          title: '首页',
          icon: 'fa-sliders'
        },
      },
    ]
  }
]

export default systemManagements;