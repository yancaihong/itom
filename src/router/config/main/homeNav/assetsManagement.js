const assetsManagement = () => import(/* webpackChunkName: "assetsManagement" */ '@/views/main/assetsManagement/index.vue');
const hardWare = () => import(/* webpackChunkName: "assetsManagement" */ '@/views/main/assetsManagement/hardWare.vue');
const softWares = () => import(/* webpackChunkName: "assetsManagement" */ '@/views/main/assetsManagement/softWares.vue');
const licenses = () => import(/* webpackChunkName: "assetsManagement" */ '@/views/main/assetsManagement/licenses.vue');
const assetInspection = () => import(/* webpackChunkName: "assetsManagement" */ '@/views/main/assetsManagement/assetInspection.vue');
const assetTree = () => import(/* webpackChunkName: "assetsManagement" */ '@/views/main/assetsManagement/assetTree.vue');
const zone = () => import(/* webpackChunkName: "assetsManagement" */ '@/views/main/assetsManagement/zone.vue');

let assetsManagements = [
  {
    path: '/assetsManagement',
    name: 'assetsManagement',
    component: assetsManagement,
    meta: {
      title: '首页',
      icon: 'fa-diamond'
    },
    children: [
      {
        path: '/assetsManagement/hardWare',
        name: 'hardWare',
        component: hardWare,
        meta: {
          title: '首页',
          icon: 'fa-server'
        },
      },
      {
        path: '/assetsManagement/softWares',
        name: 'softWares',
        component: softWares,
        meta: {
          title: '首页',
          icon: 'fa-puzzle-piece'
        },
      },
      {
        path: '/assetsManagement/licenses',
        name: 'licenses',
        component: licenses,
        meta: {
          title: '首页',
          icon: 'fa-certificate'
        },
      },
      {
        path: '/assetsManagement/assetInspection',
        name: 'assetInspection',
        component: assetInspection,
        meta: {
          title: '首页',
          icon: 'fa-bell-o'
        },
      },
      {
        path: '/assetsManagement/assetTree',
        name: 'assetTree',
        component: assetTree,
        meta: {
          title: '首页',
          icon: 'fa-sitemap'
        },
      },
      {
        path: '/assetsManagement/zone',
        name: 'zone',
        component: zone,
        meta: {
          title: '首页',
          icon: 'fa-empire'
        },
      }
    ]
  }
]

export default assetsManagements;