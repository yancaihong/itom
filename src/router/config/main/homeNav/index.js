import systemManagement from './systemManagement.js';
import business from './business.js';
import projectManagement from './projectManagement.js';
import assetsManagement from './assetsManagement.js';
import visitorsManage from './visitorsManage.js';
import ticketSystem from './ticketSystem.js';

let homeNav = [
  ...systemManagement,
  ...business,
  ...projectManagement,
  ...assetsManagement,
  ...visitorsManage,
  ...ticketSystem,
]

export default homeNav;