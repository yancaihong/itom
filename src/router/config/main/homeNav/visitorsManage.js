const visitorsManage = () => import(/* webpackChunkName: "visitorsManage" */ '@/views/main/visitorsManage/index.vue');
const visitorsRequest = () => import(/* webpackChunkName: "visitorsManage" */ '@/views/main/visitorsManage/visitorsRequest.vue');
const verifyVisitors = () => import(/* webpackChunkName: "visitorsManage" */ '@/views/main/visitorsManage/verifyVisitors.vue');

let visitorsManages = [
  {
    path: '/visitorsManage',
    name: 'visitorsManage',
    component: visitorsManage,
    meta: {
      title: '首页',
      icon: 'fa-street-view'
    },
    children: [
      {
        path: '/visitorsManage/visitorsRequest',
        name: 'visitorsRequest',
        component: visitorsRequest,
        meta: {
          title: '首页',
          icon: 'fa-angle-double-right'
        },
      },
      {
        path: '/visitorsManage/verifyVisitors',
        name: 'verifyVisitors',
        component: verifyVisitors,
        meta: {
          title: '首页',
          icon: 'fa-angle-double-right'
        },
      }
    ]
  }
]

export default visitorsManages;