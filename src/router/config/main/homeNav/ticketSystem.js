const ticketSystem = () => import(/* webpackChunkName: "ticketSystem" */ '@/views/main/ticketSystem/index.vue');
const applyTicket = () => import(/* webpackChunkName: "ticketSystem" */ '@/views/main/ticketSystem/applyTicket.vue');
const ticketSupport = () => import(/* webpackChunkName: "ticketSystem" */ '@/views/main/ticketSystem/ticketSupport.vue');

let ticketSystems = [
  {
    path: '/ticketSystem',
    name: 'ticketSystem',
    component: ticketSystem,
    meta: {
      title: '首页',
      icon: 'fa-support'
    },
    children: [
      {
        path: '/ticketSystem/applyTicket',
        name: 'applyTicket',
        component: applyTicket,
        meta: {
          title: '首页',
          icon: 'fa-support'
        },
      },
      {
        path: '/ticketSystem/ticketSupport',
        name: 'ticketSupport',
        component: ticketSupport,
        meta: {
          title: '首页',
          icon: 'fa-ticket'
        },
      }
    ]
  }
]

export default ticketSystems;