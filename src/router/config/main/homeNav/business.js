const business = () => import(/* webpackChunkName: "business" */ '@/views/main/business/index.vue');
const customers = () => import(/* webpackChunkName: "business" */ '@/views/main/business/customers.vue');
const providers = () => import(/* webpackChunkName: "business" */ '@/views/main/business/providers.vue');

let Business = [
  {
    path: '/business',
    name: 'business',
    component: business,
    meta: {
      title: '首页',
      icon: 'fa-suitcase'
    },
    children: [
      {
        path: '/business/customers',
        name: 'customers',
        component: customers,
        meta: {
          title: '首页',
          icon: 'fa-angle-right'
        },
      },
      {
        path: '/business/providers',
        name: 'providers',
        component: providers,
        meta: {
          title: '首页',
          icon: 'fa-angle-right'
        },
      }
    ]
  }
]

export default Business;