const projectManagement = () => import(/* webpackChunkName: "projectManagement" */ '@/views/main/projectManagement/index.vue');
const projects = () => import(/* webpackChunkName: "projectManagement" */ '@/views/main/projectManagement/projects.vue');
const purchaseOrders = () => import(/* webpackChunkName: "projectManagement" */ '@/views/main/projectManagement/purchaseOrders.vue');
const contracts = () => import(/* webpackChunkName: "projectManagement" */ '@/views/main/projectManagement/contracts.vue');
const projectGraph = () => import(/* webpackChunkName: "projectManagement" */ '@/views/main/projectManagement/projectGraph.vue');

let projectManagements = [
  {
    path: '/projectManagement',
    name: 'projectManagement',
    component: projectManagement,
    meta: {
      title: '首页',
      icon: 'fa-puzzle-piece'
    },
    children: [
      {
        path: '/projectManagement/projects',
        name: 'projects',
        component: projects,
        meta: {
          title: '首页',
          icon: 'fa-puzzle-piece'
        },
      },
      {
        path: '/projectManagement/purchaseOrders',
        name: 'purchaseOrders',
        component: purchaseOrders,
        meta: {
          title: '首页',
          icon: 'fa-paw'
        },
      },
      {
        path: '/projectManagement/contracts',
        name: 'contracts',
        component: contracts,
        meta: {
          title: '首页',
          icon: 'fa-openid'
        },
      },
      {
        path: '/projectManagement/projectGraph',
        name: 'projectGraph',
        component: projectGraph,
        meta: {
          title: '首页',
          icon: 'fa-bar-chart-o'
        },
      }
    ]
  }
]

export default projectManagements;