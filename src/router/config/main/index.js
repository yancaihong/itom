const Home = () => import( /* webpackChunkName: "Home" */ '@/views/main/home.vue');

import homeNav from './homeNav/index.js';
import saas from './saas.js';


let main = [{
  path: '/',
  name: 'home',
  redirect: '/systemManagement',
  component: Home,
  meta: {
    title: '首页'
  },
  children: [
    ...homeNav,
    ...saas
  ]
}]

export default main;