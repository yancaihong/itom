const saas = () => import(/* webpackChunkName: "saas" */ '@/views/main/saas/index.vue');
const myAccount = () => import(/* webpackChunkName: "saas" */ '@/views/main/saas/myAccount.vue');
const mySetting = () => import(/* webpackChunkName: "saas" */ '@/views/main/saas/mySetting.vue');

let Saas = [
  {
    path: '/saas',
    name: 'saas',
    component: saas,
    meta: {
      title: '首页'
    },
    children: [
      {
        path: '/saas/myAccount',
        name: 'myAccount',
        component: myAccount,
        meta: {
          title: '首页'
        },
      },
      {
        path: '/saas/mySetting',
        name: 'mySetting',
        component: mySetting,
        meta: {
          title: '首页'
        },
      }
    ]
  }
]

export default Saas;