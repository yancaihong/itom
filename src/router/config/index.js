import login from './login/index';
import main from './main/index'
import demo from './demo/index'

const route = [
  ...login,
  ...main,
  ...demo
]

export default route;