import {
    imageMap
} from '@/tools/config'
export function imageFilter(value) {
    return imageMap.get(value);
}