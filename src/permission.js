// 路由守卫
import router from './router';
router.beforeEach((to, from, next) => {
  console.log(to)
  if (to.meta.titleEN&&to.meta.titleZH) {
    if(window.localStorage.getItem('I18N')==='en'){
      document.title = to.meta.titleEN
    }else {
      document.title = to.meta.titleZH
    }
   
  }
  next(); // 正常跳转页面
});
