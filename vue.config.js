module.exports = {
  devServer: {
    host: "0.0.0.0",
    port: '8080',
    proxy: {
      '^/itom': {
        //target: 'http://127.0.0.1:8888', //对应自己的接口
        target: 'http://139.159.190.139:82', //对应自己的接口
        changeOrigin: true,
        // ws: true, //是否开启websocket
        pathRewrite: {
          // '^/itom': '/itom'
        },
      },
      '^/api': {
        //target: 'http://127.0.0.1:8888', //对应自己的接口
        target: 'https://itom.hopesen.com.cn', //对应自己的接口
        changeOrigin: true,
        // ws: true, //是否开启websocket
        pathRewrite: {
          // '^/api': '/api'
        }
      }
    }
  },
  // 配置网站 icon 图标
  pwa: {
    iconPaths: {
        favicon32: 'favicon1.ico',
        favicon16: 'favicon1.ico',
        appleTouchIcon: 'favicon1.ico',
        maskIcon: 'favicon1.ico',
        msTileImage: 'favicon1.ico'
    }
  }
}